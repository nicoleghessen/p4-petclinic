# **Petclinic - Docker Images deployed onto Kubernetes Cluster**

### Pre requisites

- K8 control panel connected to Jenkins
- Jenkins Master and Agent node
- 2 K8 nodes to run Petclinic

--------------------------------------------------

## **Dockerfile for Petclinic**

To see our Dockerfile for Petclinic
[click here](https://bitbucket.org/nicoleghessen/p4-petclinic/src/master/frontend/Dockerfile)

Our Petclinic images are also located [here](https://hub.docker.com/r/devopsnicole/petclinic-p4-jsn/tags). 

## **Build both Petclinic and Postgres containers.**

Our current Petclinic is using a H2 in memory database, however if you would like to configure a Postgres database to add another layer of persistence you will need to clone this repository to use [docker-compose.yml](https://bitbucket.org/nicoleghessen/p4-petclinic/src/master/docker-compose.yml)

Once you have cloned this repository you can run: 
```bash
 $ docker compose up 
```

Note: you will be able to see Petclinic working on **localhost:8080** 

### **Postgres**

We have configured a Postgres database which holds the petclinic data, this will run in its own container. The postgres database is persistent using volumes and you can check this by:

```bash

# SSH into the Postgres container
$ docker exec -it p4-petclinic-postgres-1 /bin/bash

# log into postgres with user petclinic
$ psql -U petclinic

# Switch to petclinic database
$ \c

# Show tables 
$ \dt

# Check registered vets
$ SELECT * FROM vets;

# Add a 7th vet
$ INSERT INTO vets VALUES (7, 'Michael', 'Miller') ON CONFLICT (id) DO NOTHING;

# Check 7th vet was added successfully
$ SELECT * FROM vets;

# On local terminal remove containers
$ docker compose down

# Re add the containers
$ docker compose up -d

# Once SSH back into container and logged into to Postgres and Petclinic database
$ SELECT * FROM vets;

```

## Who to contact

All feedback is welcome and you can contact either James, Shannon or Nicole at:  

- **james.lok@automationlogic.com**
- **shannon.tamplin@automationlogic.com**
- **nicole@automationlogic.com**







