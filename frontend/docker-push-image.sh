# increments build version
version=`cat build_version`   
version=$(($version + 1))

# builds docker image
docker build . -t petclinic:v$version

# retags and pushes it to dockerhub with a specific version
docker tag petclinic:v$version devopsnicole/petclinic-p4-jsn:v$version
docker push devopsnicole/petclinic-p4-jsn:v$version

# # retags and pushes it to dockerhub with latest tag to be used by our k8 cluster
docker tag petclinic:v$version devopsnicole/petclinic-p4-jsn:latest
docker push devopsnicole/petclinic-p4-jsn:latest

echo $version > build_version