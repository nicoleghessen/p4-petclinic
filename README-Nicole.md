## Load Balancer and Target Group for Petclinic

The target group has to point towards the nodeport that we have set in your nodeport-pc.yaml document

The nodes which are connected to the target group are the Kubernetes AWS Node A and B machines. 

The aim of the load balancer we have created is to manage the amount of traffic that will be on your Petclininc site. It ensures a more stable and reliable service with the likeliness of downtime being reduced. 


Go to the AWS console and make sure your K8 Control Panel, Node A and B instances are running. 

These will be called "jsn-k8-controlpanel, jsn-k8-node-a and jsn-k8-node-b".

On the left navigate to the "Load Balancers" section and select the load balancer we have created for you called "ng-k8-lb".

Select it and on the bottom go to the tab "Listeners".

You will see one "Listener ID" below called "HTTP: 80" and under the Rules column it will say "
Default: forwarding to TG-ng-k8-cluster
View/edit rules"

Click on that name which will take you to the Target Group (TG) set up.

Once this navigates you to the "Target Groups" section of the console you can see one group listed which has been created for your Petclininc service. This will be called "TG-ng-k8-cluster".

If you select this TG and look at the tabs below you will see one next to Details called "Targets".

Select "Targets" so that we can ensure the Target Group is pointing to the correct NodePort for the service.

When you click on Targets it will list the Registered Targets.

If you need to change the NodePort from the 30007 that we have set for any reason, this is how it should be done.

On the right select "Register Targets" and it will then give you a list of available instances to choose from. In the search bar type "jsn-k8- ..." and select Node A.

Select the jsn-k8-node-a machine and then below where it says "Ports for the selected instances" write the number you defined for the NodePort. This will be 30007 if you leave it on the one we have set up.

Once you type the desired figure select "include as pending below"

Go back to the search bar and repeat the same step for Node B.

Then on the bottom right press "Register pending targets"

You can then go back to the main TG-ng-k8-cluster page and go to the tab Targets to see the node instances and the port they're pointing to (remember this must match the same as the one you have set for the NodePort).








## Docker Registry 

[Click here for the Docker Hub Registry](https://hub.docker.com/r/devopsnicole/petclinic-p4-jsn/tags)

Prerequisite 
- Docker installed on the instance
- An account on Docker Hub ( I will provide the login details for you in a private document)
- A Docker Hub Repo made 

Once you have the repository made on Docker Hub you can then tag your image and then push it to the repository 
```bash
$ docker tag local-image:tagname new-repo:tagname
$ docker push new-repo:tagname
```

If you then want to use the image on your repo on a new instance you can use the pull command:
```bash

$ docker pull devopsnicole/petclinic-p4-jsn

```

## Jenkins

CI pipelines have been set up for both Petclinic and Wordpress so that you can test your code and if it's successful it will then be merged to the master branch on Bitbucket.

CD pipelines will then be triggered once a successful CI has been merged and then push the Petclinic image to the Kubernetes pods.
The same will happen for Wordpress however, the image of Wordpress will be pulled from the main Docker Hub Registry not the repository that is made specifically for you.

Ultimately- Both images will be pulled and then pushed to the Kubernetes pods. 


## Adjusting number of deployment pods in Kubernetes 
If you want to scale your business you can edit the amount of pods that are running in Kubernetes by adjusting the `Replicas` section in the 'pc-deployment.yaml' file.

Currently there are 3 but you can adjust this accordingly. 

Login details for Jenkins:

Username: jsn-jenkins 
Password: jsn-jenkins 



- if you have any issues please contact one of us from the AL team via email: nicole@automationlogic.com, shannon.tamplin@automationlogic.com or james.lok@automationlogic.com
